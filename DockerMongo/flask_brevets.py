"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import request, flash
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

SUBMITTED = False
###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    dist = int(request.args.get('dist', 200))

    dateTime = request.args.get('dateTime','help')
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    open_time = acp_times.open_time(km, dist, dateTime)
    close_time = acp_times.close_time(km, dist, dateTime)
    result = {"open": open_time, "close": close_time}
    
    return flask.jsonify(result=result)

@app.route("/_submit_data", methods=['POST'])
def submitData():
    print("DATA RECEIVED YES YES")

    data = request.get_json()
    openList = {'timesList':data['openList'], 'tag':'open'}
    closeList = {'timesList':data['closeList'], 'tag':'close'}
    distanceList = {'distanceList':data['distanceList'], 'tag':'distance'}
    SUBMITTED = True
    #Remove old collections. 
    db.tododb.remove()
    db.tododb.insert(openList)
    db.tododb.insert(closeList)
    db.tododb.insert(distanceList)
    
    print([item for item in db.tododb.find()])
    return flask.jsonify(data=data)

@app.route("/_return_data")
def returnData():
    print("DATA REQUEST RECEIVED YES YES")
    try:
       openList = db.tododb.find_one({'tag':'open'})['timesList']
       closeList = db.tododb.find_one({'tag':'close'})['timesList']
       distanceList = db.tododb.find_one({'tag':'distance'})['distanceList']

       result = {"open": openList, "close": closeList, "distance":distanceList}
       return flask.jsonify(result=result)
    except TypeError:
       flash(u'ERROR! Could not find input. Are you sure you typed an control distance?')
       errorValue = 0
       result = {"open": errorValue, "close": errorValue, "distance":errorValue}
       return flask.jsonify(result=result)
    
    
    
       
    
#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
