# Project 5: Brevet time calculator with Ajax and MongoDB

Author: Shahriar Rahman
Contact:shahriar@uoregon.edu
2020-05-22

ACP sanctioned brevet calculator that calculates the control point timing based on distances given. It works for values up to 1000km and calculates the time based on the rusa table of minimum and maximum speeds allowed. The calculator works in increments such that the first 200km of a control point will use a certain speed to calculate time and so on for the next 200km. It uses the french variation for start times such that for distances less than 60km, the closing time will follow the french rules. 


Test cases:

1. A standard entry of values like 20, 30, 50 60 values should instantly return the open time values like 0:35, 0:53, 1:28, 1:46. After entering the values, press the send data button to record to the values. Then change the km fields to different values(Any values). Ajax should automatically update the control times. Click the "Return Data" button and check results

Expected Behaviour: After clicking the "Return Data" button, the fields should revert back to the original opening and closing times except miles. Although the database will record the km distances, the code does not reconvert miles

2. Click the "Send Data" button with nothing in the fields of both km and open and closing. (May want to restart the program or do this first). This will record all the fields as empty. Click the "Return Data" button

Expected Behaviour: Should render all fields blank except miles. 

